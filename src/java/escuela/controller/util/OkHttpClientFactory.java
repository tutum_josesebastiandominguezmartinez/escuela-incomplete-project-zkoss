/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package escuela.controller.util;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
/**
 *
 * @author josesebastiandominguezmartinez
 */

public class OkHttpClientFactory {
    public static OkHttpClient createClient(String apiKey) {
        return new OkHttpClient.Builder()
                .addInterceptor(new ApiKeyInterceptor(apiKey))
                .build();
    }

    private static class ApiKeyInterceptor implements Interceptor {
        private final String apiKey;

        public ApiKeyInterceptor(String apiKey) {
            this.apiKey = apiKey;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Request newRequest = originalRequest.newBuilder()
                    .header("Api-Key", apiKey)
                    .build();
            return chain.proceed(newRequest);
        }
    }
}

