/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package escuela.controller;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;

/**
 *
 * @author josesebastiandominguezmartinez
 */
public class ControladorBase extends GenericForwardComposer{

    @Override
    public void doAfterCompose(Component comp) throws Exception {
          // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
          super.doAfterCompose(comp);
    }
    
}
