/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package escuela.controller.api;
import escuela.controller.util.OkHttpClientFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *
 * @author josesebastiandominguezmartinez
 */

public class ApiClient {
    private static final String BASE_URL = "http://localhost:8001"; // Reemplaza con la URL base de tu API

    public static ApiService createService(String apiKey) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(OkHttpClientFactory.createClient(apiKey))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ApiService.class);
    }
}
