/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package escuela.controller.api;

import escuela.model.Calificacion;
import escuela.model.Materia;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.*;
/**
 *
 * @author josesebastiandominguezmartinez
 */

public interface ApiService {
    @GET("/materia")
    Call<List<Materia>> getMaterias(@Header("Api-Key") String apiKey);
    
    //@GET("/calificacion/{id}")
    //Call<List<Calificacion>> getEndpoint(@Header("Api-Key") String apiKey, @Path("id") int id);

    /*@POST("/api/endpoint")
    Call<YourResponseClass> createEndpoint(@Header("Api-Key") String apiKey, @Body YourRequestClass request);

    @PUT("/api/endpoint/{id}")
    Call<YourResponseClass> updateEndpoint(@Header("Api-Key") String apiKey, @Path("id") String id, @Body YourRequestClass request);

    @DELETE("/api/endpoint/{id}")
    Call<Void> deleteEndpoint(@Header("Api-Key") String apiKey, @Path("id") String id);*/
}
