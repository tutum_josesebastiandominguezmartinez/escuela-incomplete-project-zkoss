/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package escuela.controller;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import escuela.controller.api.ApiClient;
import escuela.controller.api.ApiService;
import escuela.model.Calificacion;
import escuela.model.Materia;
import java.io.IOException;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zul.Button;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import retrofit2.Call;
import retrofit2.Response;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 *
 * Controla al index.zul
 *
 * @author josesebastiandominguezmartinez
 */
public class IndexControlador extends ControladorBase {

    protected Window ventana;
    protected Button btnEnviar;
    protected Textbox txtNombre;
    protected Label lbNombre;
    protected Image img;
private static final String API_URL = "http://localhost:8092";
    private static final String API_KEY = "4f9e05d3-992e-4c61-956d-350e5d2ca3bc";

    public IndexControlador() {
        System.out.println("Desde Constructor");
    }

    public void onCreate$ventana() throws IOException {
        //Unirest.setTimeouts(0, 0);
        //try {
           // HttpResponse<String> response = Unirest.get("http://localhost:8092/alumno")
            //        .header("Api-Key", "4f9e05d3-992e-4c61-956d-350e5d2ca3bc")
          //          .asString();
            
            /*OkHttpClient client = new OkHttpClient();
            
            Request request = new Request.Builder()
            .url(API_URL)
            .get()
            .addHeader("Api-Key", API_KEY)
            .build();
            
            try (okhttp3.Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
            String responseBody = response.body().string();
            System.out.println("Response: " + responseBody);
            } else {
            System.out.println("Request failed with code: " + response.code());
            }
            } catch (Exception e) {
            System.out.println("An error occurred: " + e.getMessage());
            }*/
        //} catch (UnirestException ex) {
            //Logger.getLogger(IndexControlador.class.getName()).log(Level.SEVERE, null, ex);
        //}
    



        System.out.println("aa");
        ApiService apiService = ApiClient.createService(API_KEY);
/*        System.out.println("sss");
        // Ejemplo de uso de los métodos CRUD
        Call<List<Materia>> call = apiService.getMaterias(API_KEY);
        
        Response<List<Materia>> response = call.execute();
        if (response.isSuccessful()) {
            List<Materia> calificaciones = response.body();
            // Realiza las operaciones que necesites con la lista de calificaciones
            for (Materia calificacion : calificaciones) {
                lbNombre.setValue(calificacion.getId_t_materias()+"");
                System.out.println(calificacion.getId_t_materias());
            }
        } else {
            // Maneja el caso de error en la respuesta
        }
        //System.out.println(getEndpointCall.toString());
         */
        System.out.println("Desde onCreate");
    }

    public void onClick$btnEnviar() {
        String txt = txtNombre.getText();
        //lbNombre.setValue(txt);
        System.out.println("Desde clic");
    }

    public void onChange$txtNombre() {
        String txt = txtNombre.getText();
        //lbNombre.setValue(txt + " Change");
        System.out.println(txtNombre.getText());
    }

    public void onChanging$txtNombre(InputEvent e) {
        //lbNombre.setValue(e.getValue() + " Changin");
        img.setSrc(e.getValue());
        System.out.println(e.getValue());
    }
}
