/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package escuela.model;
import java.util.Date;
/**
 *
 * @author josesebastiandominguezmartinez
 */
public class Calificacion {
    private Long id_t_usuario;
    private String nombre;
    private String apellido;
    private float calificacion;
    private Date fecha_registro;
    private String materia;
    private float promedio;
    // getters y setters

    public Long getId_t_usuario() {
        return id_t_usuario;
    }

    public void setId_t_usuario(Long id_t_usuario) {
        this.id_t_usuario = id_t_usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public float getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(float calificacion) {
        this.calificacion = calificacion;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public float getPromedio() {
        return promedio;
    }

    public void setPromedio(float promedio) {
        this.promedio = promedio;
    }
}
