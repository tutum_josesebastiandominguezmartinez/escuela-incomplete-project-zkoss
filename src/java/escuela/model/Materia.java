/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package escuela.model;


/**
 *
 * @author josesebastiandominguezmartinez
 */
public class Materia {

    private int id_t_materias;
    private String nombre;
    private int activo;

    public Materia() {
    }

    public Materia(String nombre, int activo) {
        super();
        this.nombre = nombre;
        this.activo = activo;
    }

    public int getId_t_materias() {
        return id_t_materias;
    }

    public void setId_t_materias(int id_t_materias) {
        this.id_t_materias = id_t_materias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
